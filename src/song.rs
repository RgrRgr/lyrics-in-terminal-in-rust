use std::path::Path;

use tokio::fs::create_dir_all;
use tokio::fs::File;
use tokio::io::AsyncReadExt;
use tokio::io::AsyncWriteExt;

use crate::lyrics::{Lyrics, Site};

#[derive(Debug, PartialEq, Eq, Default)]
pub struct Song {
    pub lyrics: Lyrics,
    pub name: String,
    pub album: String,
    pub artist: String,
    pub offset: usize,
}

impl Song {
    async fn get_from_file(&self) -> Result<Lyrics, ()> {
        let path = std::env::var("XDG_CACHE_HOME")
            .unwrap_or(std::env::var("HOME").unwrap() + "/.cache")
            + "/lyrics";
        let path = &format!("{}/{}-{}", path, self.artist, self.name);
        let path = Path::new(path);

        if !path.exists() {
            return Err(());
        }

        let file = File::open(path).await;
        if file.is_ok() {
            let mut file = file.unwrap();
            let mut string = String::new();
            file.read_to_string(&mut string).await.expect("Ahhh");

            return Ok(Lyrics::from_string(&string));
        } else {
            return Err(());
        }
    }

    // Artist and name get set immediately, lyrics only after this function was called
    async fn save_to_file(&self, lyrics: &Lyrics) -> Result<(), ()> {
        let base_path = std::env::var("XDG_CACHE_HOME")
            .unwrap_or(std::env::var("HOME").unwrap() + "/.cache")
            + "/lyrics";
        let path_dir = Path::new(&base_path);

        if !path_dir.exists() {
            create_dir_all(path_dir).await.unwrap();
        }

        let dir = format!("{}/{}-{}", &base_path, self.artist, self.name);
        let path = Path::new(&dir);

        if path.exists() {
            return Ok(());
        }
        // TODO set maximum number of files

        let file = File::create(path).await;
        if file.is_ok() {
            let mut file = file.unwrap();
            file.write_all(&lyrics.get_lyrics().join("\n").into_bytes())
                .await
                .unwrap();
        } else {
            return Err(());
        }

        return Ok(());
    }

    pub async fn update(&mut self, artist: &str, name: &str, album: &str) {
        self.artist = artist.to_string();
        self.name = name.to_string();
        self.album = album.to_string();
        self.offset = 0;

        let lyrics = self.get_from_file().await;
        self.lyrics = if lyrics.is_ok() {
            lyrics.unwrap()
        } else {
            let lyrics = Lyrics::new(Site::Azlyrics, artist, name).await;
            if lyrics.is_ok() {
                let lyrics = lyrics.unwrap();
                self.save_to_file(&lyrics).await.unwrap();
                lyrics
            } else {
                Lyrics::no_lyrics_found()
            }
        };
    }

    pub fn new() -> Self {
        Song {
            lyrics: Lyrics::default(),
            name: "Didn't find song".to_string(),
            artist: "Please wait".to_string(),
            album: "".to_string(),
            offset: 0,
        }
    }

    pub fn scroll(&mut self, direction: i8) {
        let positive = direction * -1;
        if direction > 0 {
            if self.offset + (direction as usize) < self.lyrics.get_lyrics().len() {
                self.offset += direction as usize;
            } else {
                self.offset = self.lyrics.get_lyrics().len();
            }
        } else if positive > 0 {
            if self.offset > positive as usize {
                self.offset -= positive as usize;
            } else {
                self.offset = 0;
            }
        }
    }
}
