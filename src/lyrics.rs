use std::{error::Error, fmt::Display};

#[derive(Debug)]
pub struct LyricsError {}

impl Display for LyricsError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "An error occured")
    }
}

impl Error for LyricsError {}

impl From<reqwest::Error> for LyricsError {
    fn from(_: reqwest::Error) -> Self {
        LyricsError {}
    }
}

impl From<tokio::io::Error> for LyricsError {
    fn from(_: tokio::io::Error) -> Self {
        LyricsError {}
    }
}

impl From<regex::Error> for LyricsError {
    fn from(_: regex::Error) -> Self {
        LyricsError {}
    }
}

#[derive(Debug, PartialEq, Eq, Default)]
pub struct Lyrics {
    text: Vec<String>,
}

pub enum Site {
    Azlyrics,
}

impl Lyrics {
    async fn get_url(artist: &str, name: &str) -> Result<String, LyricsError> {
        let artist = artist.replace(" ", "+");
        let name = name.replace(" ", "+");

        let url = format!("https://www.google.com/search?q=azlyrics+{artist}+{name}");
        let client = reqwest::Client::builder()
            .user_agent("Mozilla/5.0 Firefox/81.0")
            .build()?;

        let resp = client.get(url).send().await?.text().await?;
        let link = regex::Regex::new("(http[s]?://www.azlyrics.com/lyrics/.*\\.html)")?
            .captures_iter(&resp)
            .next();

        link.ok_or(LyricsError {}).and_then(|link| {
            let link = &link[0];
            if link.contains("\"") {
                let link = &link[0..link.find("\"").unwrap()];
                Ok(link.to_string())
            } else {
                Ok(link.to_string())
            }
        })
    }

    pub fn no_lyrics_found() -> Self {
        Lyrics {
            text: vec!["Couldn't find lyrics".to_string()],
        }
    }

    async fn get_azlyrics(artist: &str, name: &str) -> Result<Self, LyricsError> {
        let url = Lyrics::get_url(artist, name).await?;

        let client = reqwest::Client::builder()
            .user_agent("Mozilla/5.0 Firefox/81.0")
            .build()?;
        let resp = client.get(url).send().await?.text().await?;

        let beg = resp.find("<!-- Usage of azlyrics.com content by any third-party lyrics provider is prohibited by our licensing agreement. Sorry about that. -->")
            .ok_or(LyricsError{})?;
        let end = resp.find("<!-- MxM banner -->").ok_or(LyricsError {})?;

        let lyrics = resp.split_at(end).0.split_at(beg).1;
        let lyrics = lyrics.split_at(lyrics.find("\n").unwrap()).1;
        let lyrics = lyrics
            .replace("</div>", "")
            .replace("\n", "")
            .replace("&quot;", "\"");

        let lyrics: Vec<&str> = lyrics.split("<br>").map(|e| e.trim()).collect();

        let text: Vec<String> = lyrics.iter().map(|t| t.to_owned().to_owned()).collect();

        Ok(Lyrics { text })
    }

    pub async fn new(site: Site, artist: &str, name: &str) -> Result<Self, LyricsError> {
        match site {
            Site::Azlyrics => Lyrics::get_azlyrics(artist, name).await,
        }
    }

    pub fn from_string(lyrics: &str) -> Self {
        Lyrics {
            text: lyrics.split("\n").map(|str| str.to_owned()).collect(),
        }
    }

    pub fn get_lyrics(&self) -> &Vec<String> {
        &self.text
    }
}
