pub use crossterm::{
    cursor,
    event::{self, Event, KeyCode, KeyEvent},
    execute, queue, style,
    terminal::{self, ClearType},
    Command, Result,
};
use crossterm::{
    event::EventStream,
    style::{Attribute, Color, SetAttribute},
};
use mpris::PlayerFinder;
use std::{
    io::{Stdout, Write},
    time::Duration,
};

use futures::{FutureExt, StreamExt};
use futures_timer::Delay;
use tokio::select;

use crate::song::Song;

enum Alignment {
    Left,
    Center,
    Right,
}

pub struct App {
    song: Song,
    w: Stdout,
    running: bool,
    player_finder: PlayerFinder,
    player_name: Option<String>,
    dimensions: (u16, u16),
    alignment: Alignment,
}

impl App {
    pub fn new(w: Stdout, player_name: Option<String>) -> Self {
        let dimensions = crossterm::terminal::size().unwrap();
        App {
            song: Song::new(),
            w,
            running: true,
            player_finder: PlayerFinder::new().expect("Couldn't create player finder"),
            player_name,
            dimensions,
            alignment: Alignment::Center,
        }
    }

    fn get_spaces(&self, line: &str) -> (usize, usize) {
        let x = self.dimensions.0 as usize;
        let chars = line.chars().count();
        match self.alignment {
            Alignment::Left => (0, x - chars),
            Alignment::Center => {
                let a = (x - chars) / 2;
                (a, x - a - chars)
            }
            Alignment::Right => (x - chars, 0),
        }
    }

    fn redraw(&mut self) -> Result<()> {
        let (_x, y) = self.dimensions;
        self.draw_header()?;

        let lyrics = self.song.lyrics.get_lyrics();
        let offset_y = self.song.offset;
        for line in lyrics.iter().skip(offset_y).take(y as usize - 5) {
            let italic = line.contains("<i>");
            let line = if italic {
                let beg = line.find("<i>").unwrap() + 3;
                let end = line.find("</i>").unwrap();
                queue!(self.w, SetAttribute(Attribute::Italic))?;
                &line[beg..end]
            } else {
                &line[..]
            };

            let (beg, _end) = self.get_spaces(&line);
            queue!(
                self.w,
                cursor::MoveRight(beg.try_into().expect("Offset too big"))
            )?;
            queue!(self.w, style::Print(line), cursor::MoveToNextLine(1))?;
            if italic {
                queue!(self.w, SetAttribute(Attribute::Reset))?;
            }
        }

        self.w.flush()?;
        return Ok(());
    }

    fn draw_header(&mut self) -> Result<()> {
        queue!(
            self.w,
            style::ResetColor,
            terminal::Clear(ClearType::All),
            cursor::Hide,
            cursor::MoveTo(0, 0)
        )?;

        queue!(
            self.w,
            style::SetBackgroundColor(Color::DarkBlue),
            style::SetForegroundColor(Color::White)
        )?;
        let (beg, end) = self.get_spaces(&self.song.name);
        queue!(
            self.w,
            style::Print(format!("{:width$}", "", width = &beg)),
            style::Print(&self.song.name),
            style::Print(format!("{:width$}", "", width = &end)),
            cursor::MoveToNextLine(1)
        )?;
        let (beg, end) = self.get_spaces(&self.song.artist);
        queue!(
            self.w,
            style::Print(format!("{:width$}", "", width = &beg)),
            style::Print(&self.song.artist),
            style::Print(format!("{:width$}", "", width = &end)),
            cursor::MoveToNextLine(1)
        )?;
        let (beg, end) = self.get_spaces(&self.song.album);
        queue!(
            self.w,
            style::Print(format!("{:width$}", "", width = &beg)),
            style::Print(&self.song.album),
            style::Print(format!("{:width$}", "", width = &end)),
            style::ResetColor,
            cursor::MoveToNextLine(2),
        )?;

        Ok(())
    }

    async fn update_song(&mut self) {
        let get_player = || {
            if self.player_name.is_some() {
                for p in self
                    .player_finder
                    .find_all()
                    .expect("Couldn't get active players")
                    .into_iter()
                {
                    let name = p.bus_name().rfind(".").unwrap();
                    let name = &p.bus_name()[name + 1..];
                    if name == *self.player_name.as_ref().unwrap() {
                        return Some(p);
                    }
                }
                return None;
            } else {
                Some(
                    self.player_finder
                        .find_active()
                        .expect("Could not find any player"),
                )
            }
        };
        let player = get_player();

        let player = player.expect("Didn't find active player");

        let metadata = player
            .get_metadata()
            .expect("Could not get metadata for player");
        let name = metadata.title().expect("Couldn't get title");
        let artist = metadata
            .album_artists()
            .expect("Couldn't get artist")
            .concat();
        let album = metadata.album_name().expect("Couldn't get album");

        if self.song.name != name {
            self.song.update(&artist, &name, &album).await;
            self.redraw().expect("Couldn't draw to terminal");
        }
    }

    pub async fn run(&mut self) {
        let mut reader = EventStream::new();

        self.setup().await.expect("Couldn't setup alternate screen");

        while self.running {
            let event = reader.next().fuse();
            let delay = Delay::new(Duration::from_millis(2000)).fuse();
            select! {
                _ = delay => self.update_song().await,
                maybe_event = event => {
                    match maybe_event {
                        Some(Ok(event)) => {
                            if let Event::Key(KeyEvent { code: KeyCode::Char(c), .. }) = event {
                                let mut redraw = true;
                                match c {
                                    'k' => self.song.scroll(-5),
                                    'j' => self.song.scroll(5),
                                    'i' => self.alignment = Alignment::Left,
                                    'o' => self.alignment = Alignment::Center,
                                    'p' => self.alignment = Alignment::Right,
                                    'q' | 'c' => {
                                        self.running = false;
                                        redraw = false;
                                    },
                                    _ => {}
                                }
                                if redraw {
                                    self.redraw().expect("Couldn't draw to terminal");
                                }
                            }
                            if let Event::Resize(x, y) = event {
                                self.dimensions = (x, y);
                                self.redraw().unwrap();
                            }
                        }
                        Some(Err(e)) => eprintln!("Error: {:?}\r", e),
                        None => break,
                    }
                }
            }
        }

        self.putes().expect("Couldn't restore terminal");
    }

    async fn setup(&mut self) -> Result<()> {
        execute!(self.w, terminal::EnterAlternateScreen)?;

        terminal::enable_raw_mode()?;
        self.update_song().await;
        Ok(())
    }

    fn putes(&mut self) -> Result<()> {
        execute!(
            self.w,
            style::ResetColor,
            cursor::Show,
            terminal::LeaveAlternateScreen
        )?;

        terminal::disable_raw_mode()
    }
}
