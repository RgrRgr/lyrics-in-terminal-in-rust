mod app;
mod lyrics;
mod song;

use std::env::args;

use app::App;

#[tokio::main]
async fn main() {
    let stdout = std::io::stdout();
    let idx = args().enumerate().find(|arg| arg.1 == "--player");
    let mut player = None;
    if idx.is_some() {
        player = Some(args().skip(idx.unwrap().0 + 1).next().unwrap());
    }
    let mut app = App::new(stdout, player);

    app.run().await;
}
